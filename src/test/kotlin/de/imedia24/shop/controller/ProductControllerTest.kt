package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.math.BigDecimal


@WebMvcTest
class ProductControllerTest(@Autowired val mockMvc: MockMvc) {
    @MockBean
    lateinit var productService: ProductService

    private lateinit var skus: List<String>
    private lateinit var skusNotExist: List<String>
    private lateinit var products:  List<ProductResponse>

    @BeforeEach
    internal fun setUp() {
        skus = listOf("1", "2", "3")
        skusNotExist = listOf("11", "12", "13")
        products = listOf(
                ProductResponse("1", "MacBook", "MacBook Pro 2019", BigDecimal(3000)),
                ProductResponse("2", "IPhone", "iPhone XR", BigDecimal(500)),
                ProductResponse("3", "iPod", "iPod Model 2021", BigDecimal(100)),
        )
    }

    @Test
    fun givenExistingProducts_whenGetRequest_thenReturnProductJsonWithStatus200() {
        Mockito.`when`(productService.findProductBySkus(skus)).thenReturn(products);

        mockMvc.perform(get("/products")
                .param("skus", skus.joinToString(",")))
                .andDo(print())
                .andExpect(content().contentType("application/json;charset=utf-8"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$[0].name").value("MacBook"));
    }

    @Test
    fun givenNotExistingProduct_whenGetRequest_thenReturnsEmptyListWithStatus200() {
        Mockito.`when`(productService.findProductBySkus(skus)).thenReturn(products);

        mockMvc.perform(get("/products")
                .param("skus", skusNotExist.joinToString(",")))
                .andDo(print())
                .andExpect(status().isOk)
                .andExpect(jsonPath("$").isEmpty)
    }


}