package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import de.imedia24.shop.service.ProductServiceImpl
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.enums.Explode
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @Operation(summary = "Get Product information", description = "Get information for a product with id sku")
    @ApiResponses(
            value = [
                ApiResponse(responseCode = "200", description = "Successful response") ,
                ApiResponse(responseCode = "400", description = "Invalid Sku supplied"),
                ApiResponse(responseCode = "404", description = "Product with the given sku does not exist"),
                ApiResponse(responseCode = "500", description = "Server error"),
            ])
    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
            @Parameter(description = "Sku of product to return", required = true) @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @Operation(summary = "Find Product by skus", description = "Multiple skus can be provided with comma separated strings.")
    @ApiResponses(
            value = [
                ApiResponse(responseCode = "200", description = "Successful response") ,
                ApiResponse(responseCode = "400", description = "Invalid Sku value"),
                ApiResponse(responseCode = "404", description = "Product not found"),
                ApiResponse(responseCode = "500", description = "Server error"),
            ]
    )
    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductBySkus(
            @Parameter( name = "skus", description = "Skus to filter by")  @RequestParam("skus") skus: List<String>
    ): ResponseEntity<List<ProductResponse>> {
        logger.info("Request for list of products $skus")

        val products: List<ProductResponse> = productService.findProductBySkus(skus)

        return ResponseEntity.ok(products)
    }

    @Operation(summary = "Add a new product", description = "Add a new product")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "Successful response"),
        ApiResponse(responseCode = "400", description = "Invalid Sku supplied"),
        ApiResponse(responseCode = "404", description = "Product not found"),
        ApiResponse(responseCode = "405", description = "Validation exception")
    ])
    @PostMapping("/products", produces = ["application/json;charset=utf-8"])
    fun addProduct(
            @Parameter(description = "Create a new product", required = true) @Valid @RequestBody product: ProductRequest
    ): ResponseEntity<ProductResponse>  {
        logger.info("Request to Add new Product $product")

        val product = productService.saveProduct(product);
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @Operation(summary = "Update an existing product", description = "Update an existing product by sku")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "Successful response"),
        ApiResponse(responseCode = "400", description = "Invalid Sku supplied"),
        ApiResponse(responseCode = "404", description = "Product not found"),
        ApiResponse(responseCode = "405", description = "Validation exception") ])
    @PutMapping("/products", produces = ["application/json;charset=utf-8"])
    fun updateProduct(
            @Parameter(description = "Update an existent product", required = true)  @Valid @RequestBody productRequest: ProductRequest
    ): ResponseEntity<ProductResponse>  {
        logger.info("Request to update a Product $productRequest")

        val updatedProduct: ProductResponse? = productService.updateProduct(productRequest);
        return if(updatedProduct == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(updatedProduct)
        }
    }

}
