package de.imedia24.shop.service

import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductResponse

interface ProductService {
    fun findProductBySku(sku: String): ProductResponse?
    fun findProductBySkus(skus: List<String>): List<ProductResponse>
    fun saveProduct(product: ProductRequest): ProductResponse?
    fun updateProduct(product: ProductRequest): ProductResponse?
}