package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductRequest.Companion.toProductEntity
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.exception.BadRequestException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.ZonedDateTime

@Service
class ProductServiceImpl: ProductService {

    @Autowired
    lateinit var productRepository: ProductRepository

    override fun findProductBySku(sku: String): ProductResponse? {
        return productRepository.findBySku(sku)?.toProductResponse();
    }

    override fun findProductBySkus(skus: List<String>): List<ProductResponse> {
        return productRepository.findBySkuIn(skus)?.map { it.toProductResponse() };
    }

    override fun saveProduct(product: ProductRequest): ProductResponse? {
        if(product == null || product.sku == null) {
            throw IllegalArgumentException("Product or Sku cannot be null")
        }
        val existSku: Boolean = productRepository.existsBySku(product.sku);
        if(existSku) {
            throw BadRequestException("Product with Sku Id: ${product.sku} already exist");
        }
        return productRepository.save(product.toProductEntity())?.toProductResponse();
    }

    override fun updateProduct(product: ProductRequest): ProductResponse? {
        val updatedProduct: ProductEntity? = productRepository.findBySku(product.sku)

        return if(updatedProduct == null) {
            null
        } else {
            productRepository.save(updatedProduct
                    .copy(sku = product.sku)
                    .copy(name = product.name)
                    .copy(description = product.description)
                    .copy(price = product.price)
                    .copy(updatedAt = ZonedDateTime.now())).toProductResponse()
        }
    }

}
