# iMedia24 Coding challenge
iMedia24 Coding challenge Done by [Jamal ER-RAKIBI].

https://jrakibi.web.app/

## 1. Swagger Documentation
To check swagger documentation run the project locally (or from docker image) and visit http://localhost:8080/swagger-ui/index.html

## 2. Docker Contenarization
Steps to run imedia24-coding-challenge Application inside docker: 

1. Create Docker image:
```sh
$ docker build -t imedia24-coding-challenge .
```

2. Start docker container
```sh
$ docker run --name imedia24-coding-challenge-container -p 8080:8080 -d imedia24-coding-challenge
```
###### Dockerfile
```sh
FROM gradle:7.4.2-jdk8 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:8-jre-slim
RUN mkdir /app
COPY --from=build /home/gradle/src/build/libs/ /app/

ENTRYPOINT ["java","-jar","/app/shop-0.0.1-SNAPSHOT.jar"]
```

## 3. GitLab CI/CD configuration
CI/CD stages:
1. testing_packaging: 
    * Run unit test 
    * Package the project
###### .gitlab-ci.yml
```yaml
# Run unit test and package the project
Build and run unit tests:
inherit:
    default: true
    variables: true
stage: testing_packaging
script:
    - gradle clean build
```
2. build_docker_image: 
    * Build docker image for the project and 
    * Push to DockerHub
###### .gitlab-ci.yml
```yaml
# Build docker image for the project and push to DockerHub
build docker image:
image: docker:latest
stage: build_docker_image
inherit:
    default: false
    variables: false
services:
    - docker:dind
script:
    - docker build -t jamalerdocker/imedia24-coding-challenge .
    - docker save jamalerdocker/imedia24-coding-challenge > imedia24-coding-challenge.tar
    - docker login -u jamalerdocker -p $DOCKER_HUB_PASSWORD
    - docker push jamalerdocker/imedia24-coding-challenge
artifacts:
    paths:
    - imedia24-coding-challenge.tar
```


3. run_container: 
    * Run the container
    * Execute some CURL requests
###### .gitlab-ci.yml
```yaml
# Run the container and Execute some CURL requests
run imedia24-coding-challenge image:
image: docker:latest
stage: run_container
inherit:
    default: false
    variables: false
before_script:
    - apk add --update curl && rm -rf /var/cache/apk/*
services:
    - docker:dind
script:
    - docker load < imedia24-coding-challenge.tar
    - docker run --name imedia24-coding-challenge-container -p 8080:8080 -d  jamalerdocker/imedia24-coding-challenge
    - sleep 30
    - docker logs imedia24-coding-challenge-container
    - 'curl -H "Content-Type: application/json;charset=utf-8" -d @./requests/post_new_product.json -X POST http://docker:8080/products'
    - 'curl -H "Content-Type: application/json;charset=utf-8" -X GET http://docker:8080/products/1'
```

## 4. Deploy on AWS
TODO


[Jamal ER-RAKIBI]: https://jrakibi.web.app/
